'use strict'
const Task = use('App/Models/Task')
const User = use('App/Models/User')
const { validate } = use('Validator')


class TaskController {


    async index ({ auth, view, response }) {
      
  
      if (!auth.user) {
        return response.redirect('/login')
      }
      
      const tasks = await Task
      .query()
      .where('user_id', auth.user.id)
      .fetch()

     
    return view.render('tasks.index', { tasks: tasks.toJSON() })
    }


    async store ({ request, response, auth, session }) {

  

        // validate form input
        const validation = await validate(request.all(), {
          title: 'required|min:3|max:255'
        })
      
        // show error messages upon validation fail
        if (validation.fails()) {
          session.withErrors(validation.messages())
                  .flashAll()
      
          return response.redirect('back')
        }
      
        // persist to database
        const task = new Task()
        task.title = request.input('title')
        task.user_id = auth.user.id
    
        await task.save()
      
        // Fash success message to session
        session.flash({ notification: 'Task added!' })
      
        return response.redirect('back')
      }

      async destroy ({ request, session, response }) {
        console.log(request.input('id'));
        
        const task = await Task.find(request.input('id'))
        await task.delete()
      
        // Fash success message to session
        session.flash({ notification: 'Task deleted!' })
      
        return response.redirect('back')
      }
    
}

module.exports = TaskController
