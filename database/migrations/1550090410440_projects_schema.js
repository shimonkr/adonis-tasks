'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectsSchema extends Schema {
  up () {
    this.create('projects', (table) => {
      table.increments()
      table.string('project_name', 60).notNullable().defaultTo("")
      table.string('project_description', 160).notNullable().defaultTo("")
      table.timestamps()

    })
  }

  down () {
    this.drop('projects')
  }
}

module.exports = ProjectsSchema
