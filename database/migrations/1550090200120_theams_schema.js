'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TheamsSchema extends Schema {
  up () {
    this.create('theams', (table) => {
      table.increments()
      table.string('theam_name', 60).notNullable().defaultTo("")
      table.string('theme_description', 160).notNullable().defaultTo("")
      table.timestamps()


    })
  }

  down () {
    this.drop('theams')
  }
}

module.exports = TheamsSchema
