'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TasksSchema extends Schema {
  up () {
    this.create('tasks', (table) => {
      table.increments()
      table.timestamps()
      table.string('title', 80).notNullable().defaultTo("")
      table.integer('user_id').unsigned()
      table.integer('project_id').unsigned()

    })

    this.alter('tasks', (table) => {
  
      
    })
  }

  down () {
    this.drop('tasks')
  }
}

module.exports = TasksSchema
