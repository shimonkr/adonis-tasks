'use strict'

const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', table => {
      table.increments()
      table.string('username', 80).notNullable().unique().defaultTo("")
      table.string('email', 254).notNullable().unique().defaultTo("")
      table.string('password', 60).notNullable().defaultTo("")
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
