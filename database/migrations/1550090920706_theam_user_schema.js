'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TheamUserSchema extends Schema {
  up () {
    this.create('theam_user', (table) => {
      table.increments()
      table.integer('user_id').unsigned()
      table.integer('theam_id').unsigned()
      table.timestamps()
    })
  }

  down () {
    this.drop('theam_users')
  }
}

module.exports = TheamUserSchema
