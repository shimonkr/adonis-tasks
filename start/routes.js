'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

// Route.on('/').render('welcome')

Route.get('/', 'TaskController.index')
Route.post('tasks', 'TaskController.store')
Route.post('deletetasks', 'TaskController.destroy')

Route.get('deleteuser/:id', 'UserController.deleteuser')
//http://127.0.0.1:3333/deleteuser/1


// Route.get('deleteuser/:id', ({ params }) => {
//   'UserController.destroy'
// })

// Those routes should be only accessible
// when you are not logged in
Route.group(() => {
  Route.get('login', 'SessionController.create')
  Route.post('login', 'SessionController.store')

  Route.get('register', 'UserController.create')
  Route.post('register', 'UserController.store')
}).middleware(['guest'])

Route.group(() => {
  Route.get('logout', 'SessionController.delete')


  
}).middleware(['auth'])

Route.post('tasks', 'TaskController.store')
Route.post('deletetasks', 'TaskController.destroy')